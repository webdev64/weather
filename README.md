Introduction
------------

This extension allows to get weather from some weather providers

Currently implemented:
- Open Weather

Installation
------------

You can install the component in the following ways:

* Via Composer (https://packagist.org/packages/phantom/weather)

1- Publish the configs after update the composer:
php artisan config:publish phantom/weather

2- You have to create an account on http://openweathermap.org/api
Then put the app_id into the config /packages/phantom/weather/open-weather.php

3- Add this service provider into providers array of your /Config/app.php
'Phantom\Weather\WeatherServiceProvider',

Usage
-----
Create the object
```php
    $weather = \App::make('open-weather');
```

Get by city name:
```php
	$weather->getCurrentByCityName('Lisbon', 'PT');
	$weather->getDailyByCityName($city_name, $county_code,$days_count);
	$weather->getHourlyByCityName($city_name, $county_code,$hours_count));
```

By city id:
```php
    $weather->getCurrentById($id);
    $weather->getDailyById($id,$days_count);
    $weather->getHourlyById($id,$hours_count));
```

By city coordinates:
```php
    $weather->getCurrentByCoordinates($lat, $lon);
    $weather->getDailyByCoordinates($lat, $lon,$days_count);
    $weather->getHourlyByCoordinates($lat, $lon,$hours_count));
```

By city zip code:
```php
    $weather->getCurrentByZipCode($zip_code, $country_code);
```

If there is an error the function will throw it, use try catch and do whatever you want with it.