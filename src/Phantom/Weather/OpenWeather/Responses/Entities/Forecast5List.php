<?php namespace Phantom\Weather\OpenWeather\Entities;

class Forecast5List{
	/**
	 * Time of data forecasted
	 * @var integer
	 */
	public $dt;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Main
	 */
	public $main;

	/**
	 * More info Weather into condition codes
	 * @var \Phantom\Weather\OpenWeather\Entities\Weather[]
	 */
	public $weather;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Clouds
	 */
	public $clouds;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Wind
	 */
	public $wind;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Rain
	 */
	public $rain;

	/**
	 *  @var \DateTime
	 */
	public $dt_txt;

}