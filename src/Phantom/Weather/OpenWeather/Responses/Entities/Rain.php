<?php namespace Phantom\Weather\OpenWeather\Entities;

class Rain{
	/**
	 * Rain volume for the last 3 hours (mm)
	 * @var float
	 */
	public $_3h;

	/**
	 * @param float $val
	 */
	public function set3h($val){ $this->_3h = $val;}
}