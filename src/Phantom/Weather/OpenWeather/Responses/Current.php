<?php namespace Phantom\Weather\OpenWeather\Responses;

use Phantom\Weather\Base\ResponseInterface;

class Current implements ResponseInterface{
	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Coord
	 */
	public $coord;

	/**
	 * More info Weather into condition codes
	 * @var \Phantom\Weather\OpenWeather\Entities\Weather[]
	 */
	public $weather;

	/**
	 * Internal parameter
	 * @var string
	 */
	public $base;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Main
	 */
	public $main;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Wind
	 */
	public $wind;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Clouds
	 */
	public $clouds;

	/**
	 * Time of data forecasted
	 * @var integer
	 */
	public $dt;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Sys
	 */
	public $sys;

	/**
	 * City ID
	 * @var integer
	 */
	public $id;

	/**
	 * City name
	 * @var string
	 */
	public $name;

	/**
	 * Internal parameter
	 * @var integer
	 */
	public $cod;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Rain
	 */
	public $rain;
}