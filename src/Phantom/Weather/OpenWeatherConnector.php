<?php namespace Phantom\Weather;

use Illuminate\Config\Repository;
use Phantom\Weather\Base\Connector;
use Phantom\Weather\OpenWeather\Responses\Current;
use Phantom\Weather\OpenWeather\Responses\Forecast16;
use Phantom\Weather\OpenWeather\Responses\Forecast5;

class OpenWeatherConnector extends Connector {

	#Private config section
	private $params = [];
	private $url = [];

	/**
	 * The constructor
	 */
	public function __construct(Repository $config)
	{
		$this->params['APPID']   = $config->get('weather::open-weather.app_id');
		$this->params['type']   = $config->get('weather::open-weather.search_type');
		$this->params['units']   = $config->get('weather::open-weather.units');

		$this->url += [
			'current' => $config->get('weather::open-weather.base_path').'/weather',
			'daily' => $config->get('weather::open-weather.base_path').'/forecast/daily',
			'hourly' => $config->get('weather::open-weather.base_path').'/forecast/hourly'
		];
	}

	/**
	 * @param $city_name
	 * @param $county_code
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Current
	 * @throws \Exception
	 */
	public function getCurrentByCityName($city_name, $county_code)
	{
		$params = $this->params + [
			'q' => "$city_name,$county_code"
		];

		return $this->respond($this->url['current'], $params, new Current());
	}

	/**
	 * @param $city_name
	 * @param $county_code
	 * @param int $days_count 1 to 16
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Forecast16
	 * @throws \Exception
	 */
	public function getDailyByCityName($city_name, $county_code, $days_count)
	{
		$params = $this->params + [
			'q' => "$city_name,$county_code",
			'cnt' => $days_count
		];

		return $this->respond($this->url['daily'], $params, new Forecast16());
	}

	/**
	 * @param $city_name
	 * @param $county_code
	 * @param int $hours_count 1 to 37
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Forecast5
	 * @throws \Exception
	 */
	public function getHourlyByCityName($city_name, $county_code, $hours_count)
	{
		$params = $this->params + [
			'q' => "$city_name,$county_code",
			'cnt' => $hours_count
		];

		return $this->respond($this->url['hourly'], $params, new Forecast5());
	}

	/**
	 * @param $city_id
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Current
	 * @throws \Exception
	 */
	public function getCurrentById($city_id){
		$params = $this->params + [
			'id' => "$city_id"
		];

		return $this->respond($this->url['current'], $params, new Current());
	}

	/**
	 * @param $city_id
	 * @param int $days_count 1 to 16
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Forecast16
	 * @throws \Exception
	 */
	public function getDailyById($city_id, $days_count){
		$params = $this->params + [
			'id' => "$city_id",
			'cnt' => $days_count
		];

		return $this->respond($this->url['daily'], $params, new Forecast16());
	}

	/**
	 * @param $city_id
	 * @param int $hours_count 1 to 37
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Forecast5
	 * @throws \Exception
	 */
	public function getHourlyById($city_id, $hours_count){
		$params = $this->params + [
			'id' => "$city_id",
			'cnt' => $hours_count
		];

		return $this->respond($this->url['hourly'], $params, new Forecast5());
	}

	/**
	 * @param $lat
	 * @param $lon
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Current
	 * @throws \Exception
	 */
	public function getCurrentByCoordinates($lat, $lon){
		$params = $this->params + [
			'lat' => "$lat",
			'lon' => "$lon"
		];

		return $this->respond($this->url['current'], $params, new Current());
	}

	/**
	 * @param $lat
	 * @param $lon
	 * @param int $days_count 1 to 16
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Forecast16
	 * @throws \Exception
	 */
	public function getDailyByCoordinates($lat, $lon,$days_count){
		$params = $this->params + [
			'lat' => "$lat",
			'lon' => "$lon",
			'cnt' => $days_count
		];

		return $this->respond($this->url['daily'], $params, new Forecast16());
	}

	/**
	 * @param $lat
	 * @param $lon
	 * @param int $hours_count 1 to 37
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Forecast5
	 * @throws \Exception
	 */
	public function getHourlyByCoordinates($lat, $lon,$hours_count){
		$params = $this->params + [
			'lat' => "$lat",
			'lon' => "$lon",
			'cnt' => $hours_count
		];

		return $this->respond($this->url['hourly'], $params, new Forecast5());
	}

	/**
	 * @param $zip_code
	 * @param $country_code
	 *
	 * @return \Phantom\Weather\OpenWeather\Responses\Current
	 * @throws \Exception
	 */
	public function getCurrentByZipCode($zip_code, $country_code){
		$params = $this->params + [
			'zip' => "$zip_code,$country_code",
		];

		return $this->respond($this->url['current'], $params, new Current());
	}

}